const getAllUserMiddleware = (request, response, next) => {
    console.log("Get ALL User Middleware");
    next();
}

const getAllUserLimitMiddleware = (request, response, next) => {
    console.log("Get ALL User limit Middleware");
    next();
}

const getAllUserSkipMiddleware = (request, response, next) => {
    console.log("Get ALL User Skip Middleware");
    next();
}

const getAllUserSortMiddleware = (request, response, next) => {
    console.log("Get ALL User Sort Middleware");
    next();
}

const getAllUserSkipLimitMiddleware = (request, response, next) => {
    console.log("Get ALL User Skip Limit Middleware");
    next();
}

const getAllUserSortSkipLimitMiddleware = (request, response, next) => {
    console.log("Get ALL User Sort Skip Limit Middleware");
    next();
}

const createUserMiddleware = (request, response, next) => {
    console.log("Create User Middleware");
    next();
}

const getDetailUserMiddleware = (request, response, next) => {
    console.log("Get Detail User Middleware");
    next();
}

const updateUserMiddleware = (request, response, next) => {
    console.log("Update User Middleware");
    next();
}

const deleteUserMiddleware = (request, response, next) => {
    console.log("Delete User Middleware");
    next();
}

module.exports = {
    getAllUserMiddleware,
    getAllUserLimitMiddleware,
    getAllUserSkipMiddleware,
    getAllUserSortMiddleware,
    getAllUserSkipLimitMiddleware,
    getAllUserSortSkipLimitMiddleware,
    createUserMiddleware,
    getDetailUserMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}
