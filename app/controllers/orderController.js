// Import thư viện Mongoose
const { request, response } = require("express");
const mongoose=require("mongoose");
//Import Module Order Model
const voucherModel=require("../model/voucherModel");
const drinkModel = require("../model/drinkModel");
const orderModel=require("../model/orderModel");
const userModel = require("../model/userModel");


const createOrder=(request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const body= request.body;
    const userId = request.params.userId
    // B2: Validate dữ liệu
    if(!body.pizzaSize){
        return response.status(400).json({
            status: "Bad Request",
            message: "pizzaSize không hợp lệ"
        })
    }
    if(!body.pizzaType){
        return response.status(400).json({
            status: "Bad Request",
            message: "pizzaType không hợp lệ"
        })
    }
    
    if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.drink)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Drink is not valid"
        })
    }
    if(!body.status){
        return response.status(400).json({
            status: "Bad Request",
            message: "status không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    let newOrder={
        _id: mongoose.Types.ObjectId(),
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher:body.voucher,
        drink:body.drink,
        status: body.status

    }
    voucherModel.create(newOrder,(error,data)=>{
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        drinkModel.findByIdAndUpdate(userId, {
            $push: {
                orders: data._id
            }
        }, err => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create order successfully",
                data: data
            })

        })
    })
}

const getAllOrder=(request,response)=>{
    //B1 chuan bi du lieu
    voucherModel.find((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Order successfully",
            data: data
        })
    })
}

const getOrderById=(request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const orderId=request.params.orderId;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    voucherModel.findById(orderId,(error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Order successfully",
            data: data
        })
    })
}

const updateOrderById=(request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const orderId=request.params.orderId;
    const body=request.body;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    if(body.pizzaSize !== undefined && body.pizzaSize.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "pizzaSize không hợp lệ"
        })
    }

    if(body.pizzaType !== undefined && body.pizzaType.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "pizzaType không hợp lệ"
        })
    }

    if (body.voucher && !mongoose.Types.ObjectId.isValid(body.voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher không hợp lệ"
        })
    }

    if (body.drink && !mongoose.Types.ObjectId.isValid(body.drink)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "drink không hợp lệ"
        })
    }

    if(body.status !== undefined && body.status.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "status không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateOrder={}
    if(body.pizzaSize !== undefined) {
        updateOrder.pizzaSize = body.pizzaSize
    }
    if(body.pizzaType !== undefined) {
        updateOrder.pizzaType = body.pizzaType
    }
    if(body.voucher !== undefined) {
        updateOrder.voucher = body.voucher
    }
    if(body.drink !== undefined) {
        updateOrder.drink = body.drink
    }
    if(body.status !== undefined) {
        updateOrder.status = body.status
    }

    voucherModel.findByIdAndUpdate(orderId,updateOrder,(error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Order successfully",
            data: data
        })
    })

}

const deleteOrderByID= (request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const orderId=request.params.orderId;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    voucherModel.findByIdAndDelete(orderId,(error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        drinkModel.findOneAndUpdate({
            orders: orderId
        }, {
            $pull: { orders: orderId }
        }, err => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(200).json({
                status: "Delete Order successfully"
            })
        })
    })
}

const createOrderOfUser= (request,response)=>{
    const body = request.body;

    if (!body.kichCo) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "kichCo is required"
        })
    }

    if (!body.loaiPizza) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "loaiPizza is required"
        })
    }

    if (!body.idLoaiNuocUong) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "idLoaiNuocUong is required"
        })
    }

    if (!body.hoTen) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "hoTen is required"
        })
    }

    if (!body.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }

    if (!body.soDienThoai) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "soDienThoai is required"
        })
    }

    if (!body.diaChi) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "diaChi is required"
        })
    }

    let voucher ="";
    let drink = "";
    
    voucherModel.findOne({maVoucher:body.idVourcher},(voucherError,voucherData)=>{
        if(voucherData){
            return voucher=voucherData._id;
        }
    })

    drinkModel.findOne({maNuocUong:body.idLoaiNuocUong},(drinkError,drinkData)=>{
        if(drinkData){
            return drink=drinkData._id;
        }
    })

    userModel.findOne({
        email:body.email
    })
        .exec((findUserError,userData)=>{
            if(findUserError){
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: findUserError.message 
                })
            }

            if(!userData){
                userModel.create({
                    _id:mongoose.Types.ObjectId(),
                    fullName:body.hoTen,
                    email:body.email,
                    address:body.diaChi,
                    phone:body.soDienThoai
                },(createUserError,createUserData)=>{
                    if(createUserError){
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: createUserError.message
                        }) 
                    }

                    const newOrder={
                        _id: mongoose.Types.ObjectId(),
                        pizzaSize: body.kichCo,
                        pizzaType: body.loaiPizza,
                        status: 'open'
                    }

                    if(voucher){
                        return newOrder.voucher=voucher;
                    }

                    if(drink){
                        return newOrder.drink=drink;
                    }

                    orderModel.create(newOrder,(createOrderError,createOrderData)=>{
                        if(createOrderError){
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: createOrderError.message
                            })
                        }

                        userModel.findByIdAndUpdate(createUserData._id,
                            {
                                $push: { orders: createOrderData._id }
                            },err=>{
                                if (err) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal server error",
                                        message: err.message
                                    })
                                }

                                return res.status(200).json(createOrderData) 
                            })
                    })
                })
            }
            else{
                const newOrder = {
                    _id: mongoose.Types.ObjectId(),
                    pizzaSize: body.kichCo,
                    pizzaType: body.loaiPizza,
                    status: 'open'
                }
                if (voucher) newOrder.voucher = voucher;

                if (drink) newOrder.drink = drink;

                orderModel.create(newOrder,
                    (createOrderError, createOrderData) => {
                        if (createOrderError) {
                            return response.status(500).json({
                                status: "Error 5004: Internal server error",
                                message: createOrderError.message
                            })
                        }

                        userModel.findByIdAndUpdate(userData._id,
                            {
                                $push: { orders: createOrderData._id }
                            },
                            err => {
                                if (err) {
                                    return response.status(500).json({
                                        status: "Error 5005: Internal server error",
                                        message: err.message
                                    })
                                }

                                return response.status(200).json(createOrderData)
                            })
                    })
            }
        })
}

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderByID,
    createOrderOfUser
}