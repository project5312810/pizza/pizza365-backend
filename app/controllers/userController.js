// Import thư viện Mongoose
const { request, response } = require("express");
const mongoose=require("mongoose");
//Import Module user Model
const userModel=require("../model/userModel");

const getAllUser=(request,response)=>{
    //B1 chuan bi du lieu
    userModel.find((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all user successfully",
            data: data
        })
    })
}

const getAllUserLimit=(request,response)=>{
    //B1 chuan bi du lieu
    //thu thap du lieu

    let limit=request.query.limit;

    
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().limit(limit).exec((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get User success",
                data: data
            })
        }

    })

}

const getAllUserSkip=(request,response)=>{
    //B1 chuan bi du lieu
    //thu thap du lieu
    let skip=request.query.skip;

    
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().skip(skip).exec((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get User success",
                data: data
            })
        }

    })

}

const getAllUserSort=(request,response)=>{
    //B1 chuan bi du lieu
    //thu thap du lieu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().sort({fullName:"asc"}).exec((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get User success",
                data: data
            })
        }

    })

}

const getAllUserSkipLimit=(request,response)=>{
    //B1 chuan bi du lieu
    //thu thap du lieu
    let skip=request.query.skip;
    let limit=request.query.limit;
    
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().skip(skip).limit(limit).exec((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get User success",
                data: data
            })
        }

    })

}

const getAllUserSortSkipLimit=(request,response)=>{
    //B1 chuan bi du lieu
    //thu thap du lieu
    let skip=request.query.skip;
    let limit=request.query.limit;
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find().sort({fullName:"asc"}).skip(skip).limit(limit).exec((error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get User success",
                data: data
            })
        }

    })

}

const createUser=(request,response)=>{
    // B1: Chuẩn bị dữ liệu

    const body= request.body;
    // B2: Validate dữ liệu
    if(!body.fullName){
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }
    if(!body.email){
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }
    if(!body.address){
        return response.status(400).json({
            status: "Bad Request",
            message: "address không hợp lệ"
        })
    }
    if(!body.phone||isNaN(body.phone)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Phone không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newUser={
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    }
    userModel.create(newUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create User successfully",
            data: data
        })
    })

}

const getUserById=(request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const userId=request.params.userId;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    userModel.findById(userId,(error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail user successfully",
            data: data
        })
    })
}

const updateUserById=(request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const userId=request.params.userId;
    const body=request.body;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userID không hợp lệ"
        })
    }

    if(body.fullName !== undefined && body.fullName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }

    if(body.email !== undefined && body.email.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }

    if(body.address !== undefined && body.address.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "address không hợp lệ"
        })
    }

    if(body.phone !== undefined && ( isNaN(body.phone))) {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }
    // if(body.orders !== undefined && body.orders.trim() === "") {
    //     return response.status(400).json({
    //         status: "Bad Request",
    //         message: "orders không hợp lệ"
    //     })
    // }
    // B3: Gọi Model tạo dữ liệu
    const updateUser={}
    if(body.fullName !== undefined) {
        updateUser.fullName = body.fullName
    }
    if(body.email !== undefined) {
        updateUser.email = body.email
    }
    if(body.address !== undefined) {
        updateUser.address = body.address
    }
    if(body.phone !== undefined) {
        updateUser.phone = body.phone
    }


    userModel.findByIdAndUpdate(userId,updateUser,(error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update user successfully",
            data: data
        })
    })

}

const deleteUserByID= (request,response)=>{
    // B1: Chuẩn bị dữ liệu
    const userId=request.params.userId;
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(userId,(error,data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete user successfully",
        })
    })
}

module.exports = {
    getAllUser,
    getAllUserLimit,
    getAllUserSkip,
    getAllUserSort,
    getAllUserSkipLimit,
    getAllUserSortSkipLimit,
    createUser,
    getUserById,
    updateUserById,
    deleteUserByID
}