// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import Drink middleware
const DrinkMiddleware = require("../middlewares/drinkMiddleware");

// Import Drink controller
const DrinkController = require("../controllers/drinkController")

router.get("/drinks", DrinkMiddleware.getAllDrinkMiddleware, DrinkController.getAllDrink);

router.post("/drinks", DrinkMiddleware.createDrinkMiddleware, DrinkController.createDrink);

router.get("/drinks/:drinkId", DrinkMiddleware.getDetailDrinkMiddleware, DrinkController.getDrinkById)

router.put("/drinks/:drinkId", DrinkMiddleware.updateDrinkMiddleware, DrinkController.updateDrinkById)

router.delete("/drinks/:drinkId", DrinkMiddleware.deleteDrinkMiddleware, DrinkController.deleteDrinkByID)

router.get("/devcamp-pizza365/drinks", DrinkMiddleware.getAllDrinkMiddleware, DrinkController.getAllDrink)

module.exports = router;