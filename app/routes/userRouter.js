// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import Users middleware
const UsersMiddleware = require("../middlewares/userMiddleware");

// Import Users controller
const UsersController = require("../controllers/userController")

router.get("/users", UsersMiddleware.getAllUserMiddleware, UsersController.getAllUser);

router.get("/limit-users", UsersMiddleware.getAllUserLimitMiddleware, UsersController.getAllUserLimit);

router.get("/skip-users", UsersMiddleware.getAllUserSkipMiddleware, UsersController.getAllUserSkip);

router.get("/sort-users", UsersMiddleware.getAllUserSortMiddleware, UsersController.getAllUserSort);

router.get("/skip-limit-users", UsersMiddleware.getAllUserSkipLimitMiddleware, UsersController.getAllUserSkipLimit);

router.get("/sort-skip-limit-users", UsersMiddleware.getAllUserSortSkipLimitMiddleware, UsersController.getAllUserSortSkipLimit);

router.post("/users", UsersMiddleware.createUserMiddleware, UsersController.createUser);

router.get("/users/:userId", UsersMiddleware.getDetailUserMiddleware, UsersController.getUserById)

router.put("/users/:userId", UsersMiddleware.updateUserMiddleware, UsersController.updateUserById)

router.delete("/users/:userId", UsersMiddleware.deleteUserMiddleware, UsersController.deleteUserByID)

module.exports = router;