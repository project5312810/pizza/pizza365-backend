// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import voucher middleware
const voucherMiddleware = require("../middlewares/voucherMiddleware");

// Import voucher controller
const voucherController = require("../controllers/voucherController")

router.get("/vouchers", voucherMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher);

router.get("/devcamp-pizza365/vouchers/:voucherId", voucherController.getVoucherByVoucherId);

router.post("/vouchers", voucherMiddleware.createVoucherMiddleware, voucherController.createVoucher);

router.get("/vouchers/:voucherId", voucherMiddleware.getDetailVoucherMiddleware, voucherController.getVoucherById)

router.put("/vouchers/:voucherId", voucherMiddleware.updateVoucherMiddleware, voucherController.updateVoucherById)

router.delete("/vouchers/:voucherId", voucherMiddleware.deleteVoucherMiddleware, voucherController.deleteVoucherByID)

module.exports = router;