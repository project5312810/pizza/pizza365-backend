// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import Orders middleware
const orderMiddleware = require("../middlewares/orderMiddleware");

// Import Orders controller
const orderController = require("../controllers/orderController")

router.post("/users/:userId/orders", orderMiddleware.createOrderMiddleware, orderController.createOrder);

router.get("/orders", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder);

router.get("/orders/:orderId", orderMiddleware.getDetailOrderByIDMiddleware, orderController.getOrderById)

router.put("/orders/:orderId", orderMiddleware.updateOrderMiddleware, orderController.updateOrderById)

router.delete("/orders/:orderId", orderMiddleware.deleteOrderMiddleware, orderController.deleteOrderByID)

module.exports = router;