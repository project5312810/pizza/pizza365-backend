const express = require("express")

const orderRouter = express.Router()

const orderController = require('../controllers/orderController')

orderRouter.post('/devcamp-pizza365/orders',orderController.createOrderOfUser)

module.exports = orderRouter