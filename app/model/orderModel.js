// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;
const randToken = require('rand-token');
// Khởi tạo 1 instance orderSchema từ class Schema
const orderSchema = new Schema({
    orderCode: {
        type: String,
        default:function() {
            return randToken.generate(16);
        },
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    vouchers: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drinks: {
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    status: {
        type: String,
        required: true
    },
}, 
{
    timestamps: true
})

module.exports = mongoose.model("Order", orderSchema);
