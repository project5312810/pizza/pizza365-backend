// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance userSchema từ class Schema
const userSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique:true,
        required:true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        unique:true,
        required:true
    },
    // Một user có nhieu order
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
}, 
{
    timestamps: true
})

module.exports = mongoose.model("User", userSchema);
