//import thu vien mongoose
const mongoose=require("mongoose");
//class Schema tu thu vien mongoose
const Schema= mongoose.Schema;
//khoi tao instance drinkSchema tu Class Schema
const drinkSchema= new Schema({
    maNuocUong:{
        type:String,
        unique:true,
        required:true
    },
    tenNuocUong:{
        type:String,
        required:true
    },
    donGia:{
        type:Number,
        required:true
    }
},{
    //ghi dau ban ghi duoc tao hay cap nhat vao thoi gian nao
    timestamps:true
});
//Bien dich Drink model tu bookSchema
module.exports=mongoose.model("Drink",drinkSchema)