//import thu vien mongoose
const mongoose=require("mongoose");
//class Schema tu thu vien mongoose
const Schema= mongoose.Schema;
//khoi tao instance voucherSchema tu Class Schema
const voucherSchema= new Schema({
    maVoucher:{
        type:String,
        unique:true,
        required:true
    },
    phanTramGiamGia:{
        type:Number,
        required:true
    },
    ghiChu:{
        type:String,
        required:false
    }
},
{
    //ghi dau ban ghi duoc tao hay cap nhat vao thoi gian nao
    timestamps:true
});
//Bien dich Voucher model tu bookSchema
module.exports=mongoose.model("Voucher",voucherSchema)