// Khai báo thư viên Express
const express = require("express");
var cors = require('cors')
// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();


// Khai báo cổng chạy app 
const port = 8000;

//khai bao model
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");
const devcampOrder = require("./app/routes/devcapmPizza356Router");

// Cấu hình request đọc được body json
app.use(express.json());
app.use(cors());
// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.use(express.static(__dirname + "/views"))
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"))
})
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365", (error) => {
    if (error) throw error;
    console.log("Connect MongoDB successfully!");
})

// App sử dụng router
app.use("/api", drinkRouter);
app.use("/api", voucherRouter);
app.use("/api", userRouter);
app.use("/api", orderRouter);
app.use("/api", devcampOrder);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})